//Determinar si dos números enteros son iguales, sin utilizar estructuras de control de
//condiciones, operadores de comparación o las operaciones de suma y resta.
//Debe imprimir los dos números enteros.
//Debe imprimir el resultado de igualdad entre estos.

fun main(Args: Array<String>){
    print("Digite un número:")
    var Val1 = readLine()?.toInt() as kotlin.Int

    print("Digite un segundo número:")
    var Val2 = readLine()?.toInt() as kotlin.Int

    println(Val1.equals(Val2))
}
        
fun main(args: Array<String>) {

    var n= 5
    var k= 3
    var nk= n-k

    cBinoA(n,k,nk)
    cBinoB(n,k)
}

fun cBinoA (n: Int , k: Int, nk: Int){
    var nFact= n
    var kFact= k
    var nkFact= nk

    //Sacar Factorial de n (n!)
    for(i in n downTo 1 step 1)
        if (i != n)
            nFact *= i

    //Sacar Factorial de k (k!)
    for(i in k downTo 1 step 1)
        if (i != k)
            kFact *= i

    //Sacar Factorial de n-k (n-k)!
    for(i in nk downTo 1 step 1)
        if (i != nk)
            nkFact *= i

    // Mostrar Solución = n!/(n-k)!*k
    val Solu =nFact/(nkFact*kFact)
    println("Metodo 1 " )
    print("Valor de n: $n \nValor de k: $k \n" )
    print("La cantidad de subconjuntos que se \npueden formar a partir de n y k son:  $Solu\n")
    println("============================" )
}

fun cBinoB (n: Int, k: Int){
    var nFact= n
    var kFact= k
    var conta = 1

    //Sacar Factorial de n (n!)
    for(i in n downTo 1 step 1)
        if(conta == k){
            break
        }else if (i != n){
            nFact *= i
            conta ++
        }


    //Sacar Factorial de k (k!)
    for(i in k downTo 1 step 1)
        if (i != k)
            kFact *= i


    val Solu =nFact/kFact
    println("Metodo 2" )
    print("Valor de n: $n \nValor de k: $k \n" )
    print("La cantidad de subconjuntos que se \npueden formar a partir de n y k son:  $Solu")

}

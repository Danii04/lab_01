import java.lang.Math.sqrt

fun disPuntos(x1: Double, y1: Double, x2: Double, y2: Double){
    println("Distancia entre Puntos")
    val Solu = sqrt(Math.pow((x2-x1),2.0) +  Math.pow((y2-y1),2.0))
    print("La distancia que existe entre los dos puntos A y B es de: $Solu")

}

//Hallar el menor de dos números enteros positivos, sin utilizar estructuras de control condicionales.
//Debe crear por lo menos dos métodos que realicen dicha tarea.
//Debe imprimir los dos números enteros.
//Debe imprimir el menor de estos conforme a sus dos métodos.*

fun main(Args: Array<String>){
    print("Digite un número positivo:")
    var Val1 = readLine()?.toInt() as kotlin.Int

    print("Digite otro número positivo:")
    var Val2 = readLine()?.toInt() as kotlin.Int

    val Menor= minOf(Val1, Val2)

    print("El menor es: $Menor")
}
